//###############################
//#       Audio Frequency       #
//# date: 5/13/2018 :: K23 ::TK #
//###############################


#include <LiquidCrystal.h>

const int LCD_NB_COLUMNS = 16;


volatile unsigned long timerCounts;
volatile boolean counterReady;

unsigned long overflowCount;
unsigned int timerTicks;
unsigned int timerPeriod;
 
const int sampleWindow = 50;                              
unsigned int sample;

//const int rs = 12, en = 11, d4 = 10, d5 = 9, d6 = 8, d7 = 7;
LiquidCrystal lcd(12, 11, 10, 9, 8, 7);

byte START_DIV_0_OF_1[8] = {
  B01111, 
  B11000,
  B10000,
  B10000,
  B10000,
  B10000,
  B11000,
  B01111
}; // Char début 0 / 1

byte START_DIV_1_OF_1[8] = {
  B01111, 
  B11000,
  B10011,
  B10111,
  B10111,
  B10011,
  B11000,
  B01111
}; // Char début 1 / 1

byte DIV_0_OF_2[8] = {
  B11111, 
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B11111
}; // Char milieu 0 / 2

byte DIV_1_OF_2[8] = {
  B11111, 
  B00000,
  B11000,
  B11000,
  B11000,
  B11000,
  B00000,
  B11111
}; // Char milieu 1 / 2

byte DIV_2_OF_2[8] = {
  B11111, 
  B00000,
  B11011,
  B11011,
  B11011,
  B11011,
  B00000,
  B11111
}; // Char milieu 2 / 2

byte END_DIV_0_OF_1[8] = {
  B11110, 
  B00011,
  B00001,
  B00001,
  B00001,
  B00001,
  B00011,
  B11110
}; // Char fin 0 / 1

byte END_DIV_1_OF_1[8] = {
  B11110, 
  B00011,
  B11001,
  B11101,
  B11101,
  B11001,
  B00011,
  B11110
}; // Char fin 1 / 1


/**
 * Fonction de configuration de l'écran LCD pour la barre de progression.
 * Utilise les caractères personnalisés de 0 à 6 (7 reste disponible).
 */
void setup_progressbar() {

  /* Enregistre les caractères personnalisés dans la mémoire de l'écran LCD */
  lcd.createChar(0, START_DIV_0_OF_1);
  lcd.createChar(1, START_DIV_1_OF_1);
  lcd.createChar(2, DIV_0_OF_2);
  lcd.createChar(3, DIV_1_OF_2);
  lcd.createChar(4, DIV_2_OF_2);
  lcd.createChar(5, END_DIV_0_OF_1);
  lcd.createChar(6, END_DIV_1_OF_1);
}


void draw_progressbar_start(byte percent) {

  /* Affiche la nouvelle valeur sous forme numérique sur la première ligne */
//  lcd.setCursor(0, 0);
//  lcd.print(percent);
//  lcd.print(F(" %  "));
  // N.B. Les deux espaces en fin de ligne permettent d'effacer les chiffres du pourcentage
  // précédent quand on passe d'une valeur à deux ou trois chiffres à une valeur à deux ou un chiffres.

  lcd.setCursor(0, 1);
 
  /* Map la plage (0 ~ 100) vers la plage (0 ~ LCD_NB_COLUMNS * 2 - 2) */
  byte nb_columns = map(percent, 0, 100, 0, LCD_NB_COLUMNS * 2 - 2);
  // Chaque caractère affiche 2 barres verticales, mais le premier et dernier caractère n'en affiche qu'une.

  /* Dessine chaque caractère de la ligne */
  for (byte i = 0; i < LCD_NB_COLUMNS; ++i) {

    if (i == 0) { // Premiére case

      /* Affiche le char de début en fonction du nombre de colonnes */
      if (nb_columns > 0) {
        lcd.write(1); // Char début 1 / 1
        nb_columns -= 1;

      } else {
        lcd.write((byte) 0); // Char début 0 / 1
      }

    } else if (i == LCD_NB_COLUMNS -1) { // Derniére case

      /* Affiche le char de fin en fonction du nombre de colonnes */
      if (nb_columns > 0) {
        lcd.write(6); // Char fin 1 / 1

      } else {
        lcd.write(5); // Char fin 0 / 1
      }

    } else { // Autres cases

      /* Affiche le char adéquat en fonction du nombre de colonnes */
      if (nb_columns >= 2) {
        lcd.write(4); // Char div 2 / 2
        nb_columns -= 2;

      } else if (nb_columns == 1) {
        lcd.write(3); // Char div 1 / 2
        nb_columns -= 1;

      } else {
        lcd.write(2); // Char div 0 / 2
      }
    }
  }
}

void draw_progressbar(float db,float frq) {

  /* Affiche la nouvelle valeur sous forme numérique sur la première ligne */
  lcd.setCursor(0, 0);
  lcd.print(db,0);
  lcd.print(F("dB   "));
  lcd.print(frq,0);
  lcd.print(F("Hz     "));




  
  
  lcd.setCursor(0, 1);
 
  /* Map la plage (0 ~ 100) vers la plage (0 ~ LCD_NB_COLUMNS * 2 - 2) */
  byte nb_columns = map(db, 0, 100, 0, LCD_NB_COLUMNS * 2 - 2);
  // Chaque caractère affiche 2 barres verticales, mais le premier et dernier caractère n'en affiche qu'une.

  /* Dessine chaque caractère de la ligne */
  for (byte i = 0; i < LCD_NB_COLUMNS; ++i) {

    if (i == 0) { // Premiére case

      /* Affiche le char de début en fonction du nombre de colonnes */
      if (nb_columns > 0) {
        lcd.write(1); // Char début 1 / 1
        nb_columns -= 1;

      } else {
        lcd.write((byte) 0); // Char début 0 / 1
      }

    } else if (i == LCD_NB_COLUMNS -1) { // Derniére case

      /* Affiche le char de fin en fonction du nombre de colonnes */
      if (nb_columns > 0) {
        lcd.write(6); // Char fin 1 / 1

      } else {
        lcd.write(5); // Char fin 0 / 1
      }

    } else { // Autres cases

      /* Affiche le char adéquat en fonction du nombre de colonnes */
      if (nb_columns >= 2) {
        lcd.write(4); // Char div 2 / 2
        nb_columns -= 2;

      } else if (nb_columns == 1) {
        lcd.write(3); // Char div 1 / 2
        nb_columns -= 1;

      } else {
        lcd.write(2); // Char div 0 / 2
      }
    }
  }
}

void startCounting (unsigned int ms) 
  {
  counterReady = false;         
  timerPeriod = ms;             
  timerTicks = 0;               
  overflowCount = 0;            


  TCCR1A = 0;             
  TCCR1B = 0;              
  TCCR2A = 0;
  TCCR2B = 0;


  TIMSK1 = bit (TOIE1);   

  
  TCCR2A = bit (WGM21) ;   
  OCR2A  = 124;            

  TIMSK2 = bit (OCIE2A);  

  TCNT1 = 0;      
  TCNT2 = 0;     

 
  GTCCR = bit (PSRASY);        
  
  TCCR2B =  bit (CS20) | bit (CS22) ;  

  TCCR1B =  bit (CS10) | bit (CS11) | bit (CS12);
  }  

ISR (TIMER1_OVF_vect)
  {
  ++overflowCount;               
  }  


ISR (TIMER2_COMPA_vect) 
  {
 
  unsigned int timer1CounterValue;
  timer1CounterValue = TCNT1;  
  unsigned long overflowCopy = overflowCount;


  if (++timerTicks < timerPeriod) 
    return;  // not yet


  if ((TIFR1 & bit (TOV1)) && timer1CounterValue < 256)
    overflowCopy++;



  TCCR1A = 0;    
  TCCR1B = 0;    

  TCCR2A = 0;    
  TCCR2B = 0;    

  TIMSK1 = 0;    
  TIMSK2 = 0;    

 
  timerCounts = (overflowCopy << 16) + timer1CounterValue;  
  counterReady = true;             
  }  

void setup () 
  {
  lcd.begin(16, 2);
  Serial.begin(115200);       
  setup_progressbar();
  Serial.println("****** Audio Frequency *******");
  lcd.setCursor(0, 0);
  lcd.print("Audio Frequency    ");
  for(int i=0;i<100;i++){
  draw_progressbar_start(i);
  delay(10);
  }
  delay(500); 
  lcd.clear();   
  } 

void loop () 
  {
  unsigned long startMillis= millis();                   // Start of sample window
  float peakToPeak = 0;
   
  unsigned int signalMax = 0;                            
  unsigned int signalMin = 1024; 

  while (millis() - startMillis < sampleWindow)
   {
      sample = analogRead(0);                             
      if (sample < 1024)                                  
         if (sample > signalMax)
         {
            signalMax = sample;                           
         }
         else if (sample < signalMin)
         {
            signalMin = sample;                           
      }
   }
   peakToPeak = signalMax - signalMin;                    
   float db = map(peakToPeak,20,900,49.5,90);             
   Serial.print ("Frequency: ");
   Serial.print(db);                                     
   Serial.println(F(" dB"));    
    

  byte oldTCCR0A = TCCR0A;
  byte oldTCCR0B = TCCR0B;
  TCCR0A = 0;    
  TCCR0B = 0;    

  startCounting (500);  

  while (!counterReady) 
     { }  

  
  float frq = (timerCounts *  1000.0) / timerPeriod;

  Serial.print ("             ");
  Serial.print ((unsigned long) frq);
  Serial.println (F(" Hz."));
  
  draw_progressbar(db,frq);
  
  TCCR0A = oldTCCR0A;
  TCCR0B = oldTCCR0B;

  
  
  }   
